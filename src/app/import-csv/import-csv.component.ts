import {
    Component,
    OnInit
} from "@angular/core";

@Component({
    selector: "app-import-csv",
    templateUrl: "./import-csv.component.html",
    styleUrls: ["./import-csv.component.css"]
})
export class ImportCsvComponent implements OnInit
{
    /**
     * TODO:
     * - Save mapping
     * - Add a button to use the first row as mapping configuration
     */
    private filename: string = "";
    private fileContent: string;
    private parsedCSV: string[][];

    private displayedRowsCount: number = 5;
    private columnCount: number = 0;
    private rowCount: number = 0;

    private mappingSet: Mapping;
    private mappingPreset: Mapping[] = PRESET;
    private currentMappingId: number;
    private currentMapping: Mapping;

    // Copy the fields array, to be able to add custom field and marked them as custom
    private FIELDS = FIELDS.map(x => x);


    constructor()
    {
        this.onMappingChange(0);

        // generate file content directly for tests
        const columns = 20;
        const rows = 20;
        let tmp: any =
            [
                [].constructor(columns)
                  .fill(0)
                  .map((x, i) => ("header-" + (i + 1)))
            ];
        tmp = tmp.concat([].constructor(rows)
                           .fill(0)
                           .map((x, i) =>
                           {
                               return ([].constructor(columns)
                                         .fill(0)
                                         .map((x, j) =>
                                         {
                                             return ("cell-" + (i + 1) + "-" + (j + 1));
                                         }));
                           }));
        tmp = tmp.map(x => x.join(","))
                 .join("\n");
        this.fileContent = tmp;
        this.parseCSV();
    }

    ngOnInit()
    {

    }

    loadMore(quantity?: number)
    {
        if (quantity > 0)
            this.displayedRowsCount += quantity;
        else
            this.displayedRowsCount = 0;
        this.parseCSV();
    }

    onChange(event): void
    {
        let file = event.target.files[0];
        let fileReader: FileReader = new FileReader();

        fileReader.onloadend = x =>
        {
            console.log(x);
            this.fileContent = fileReader.result;
            this.filename = file.name;
            this.parseCSV();
        };
        fileReader.readAsText(file);
    }

    parseCSV()
    {
        // Parse csv
        this.parsedCSV = this.fileContent.split("\n")
                             .map(function(v)
                             {
                                 let tmp = v.split(",");
                                 if (v.length > 0 && tmp.length > 0)
                                     return tmp;
                             })
                             .filter(v => !!v);

        // Limit the numbers of row
        // Just for prototyping purpose, the limit should be set at the loading of the file to avoid having huge file in memory
        if (this.displayedRowsCount > 0)
            this.parsedCSV.splice(this.displayedRowsCount);

        // Set the boundary of the table
        // The number of column is based only on the first row
        this.rowCount = this.parsedCSV.length;
        if (this.rowCount > 0)
            this.columnCount = this.parsedCSV[0].length;
    }

    saveMapping()
    {
        // let mapping = this.mappingPreset.filter(item => item.id === 0)[0];
        // if(mapping)
        //     mapping.fields
        // console.log(this.mappingPreset);
    }

    mappingExists(currentMappingId: number)
    {
        return this.mappingPreset.filter(item => item.id === currentMappingId).length;
    }

    onMappingChange(id)
    {
        this.currentMapping = JSON.parse(JSON.stringify(this.mappingPreset.filter(x => x.id === +id)[0]));
        if (this.currentMapping.fields.length < this.columnCount)
            this.currentMapping.fields = this.currentMapping.fields.concat([].constructor(this.columnCount - this.currentMapping.fields.length)
                                                                             .fill());
        this.currentMapping.fields = this.currentMapping.fields.map(item => item === undefined ? "unmapped" : item);
    }

    columnMappingStatus(columnIndex: number)
    {
        let column = this.currentMapping.fields[columnIndex];
        if (column === "unmapped" || !column)
            return "unmapped";
        if (column[column.length - 1] === ".")
            return "no-index";
        if (column.split(".").length > 1)
            return "valid-mapping";
        if (this.FIELDS.indexOf(column) === -1)
            return "unknown-mapping";
        return "valid-mapping";
    }
}


interface Mapping
{
    id: number,
    fields: string[]
}

/**
 * fields ending with a dot are repeatable by adding a number
 */
const FIELDS: string[] =
    [
        "firstName",
        "lastName",
        "name",
        "language", "language.",
        "address", "address.",
        "specialty", "specialty."
    ];

const PRESET: Mapping[] =
    [
        {
            id: 0,
            fields:
                [
                    "firstName",
                    "lastName",
                    "specialty"
                ]
        },
        {
            id: 1,
            fields:
                [
                    "firstName",
                    "lastName",
                    "specialty",
                    "language.1",
                    "language.2"
                ]
        },
        {
            id: 2,
            fields:
                [
                    "name",
                    "city",
                    "address",
                    "specialty.1",
                    "specialty.2",
                    "specialty.3"
                ]
        }
    ];
